package com.example.feature_book_nerd.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.feature_book_nerd.model.local.entity.Book

@Dao
interface BookDao {

    @Query("SELECT * FROM book")
    suspend fun getAll():List<Book>

    @Insert
    suspend fun insert(book: List<Book>)

//    @Update
//    suspend fun update(book: Book)
}