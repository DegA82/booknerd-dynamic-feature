package com.example.feature_book_nerd.model.di

import com.example.feature_book_nerd.model.remote.BookService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(ViewModelComponent::class)
object BookModule {

    @Provides
    fun providesBookService(): BookService= BookService.getInstance()
}