package com.example.feature_book_nerd.model.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.feature_book_nerd.model.BookNerdRepo
import com.example.feature_book_nerd.model.local.entity.Book
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
@HiltViewModel
class BookViewModel @Inject constructor(
    private val repo : BookNerdRepo): ViewModel() {

    val state: LiveData<BookState> = liveData {
        emit(BookState(isLoading=true))
        val books = repo.getBooks()
        emit(BookState(books = books))
    }

    data class BookState(
        val isLoading: Boolean = false,
        val books: List<Book> = emptyList()
    )

    class BookViewModelFactory(
//        private val bookNerdRepo: BookNerdRepo
        val bookViewModel: BookViewModel
    ): ViewModelProvider.NewInstanceFactory(){
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return bookViewModel as T
        }
    }

}